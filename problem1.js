//problem1.js
function findUsersInterestedInVideoGames(users) {
    const interestedUsers = [];

    try {
        const userNames = Object.keys(users);
        
        for (let i = 0; i < userNames.length; i++) {
            const userName = userNames[i];
            const user = users[userName];
            
            // Check if the user has interests and includes "Playing Video Games"
            if (user.interests && user.interests.includes("Playing Video Games")) {
                interestedUsers.push(userName);
            }
        }
    } catch (error) {
        console.error("Error while finding users interested in video games:", error);
    }
    return interestedUsers;
}

module.exports = { findUsersInterestedInVideoGames };
