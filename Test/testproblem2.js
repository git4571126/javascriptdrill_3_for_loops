const { findUsersInGermany } = require('../problem2');
const users = require('./dataset');

console.log("Users staying in Germany:");
const usersInGermany = findUsersInGermany(users);
console.log(usersInGermany);
