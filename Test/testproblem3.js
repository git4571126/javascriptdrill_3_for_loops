const { findUsersWithMasters } = require('../problem3');
const users = require('./dataset');

console.log("Users with Masters degree:");
const usersWithMasters = findUsersWithMasters(users);
console.log(usersWithMasters);
