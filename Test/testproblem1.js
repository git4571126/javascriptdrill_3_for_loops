const { findUsersInterestedInVideoGames } = require('../problem1');
const users = require('./dataset');
console.log("Users interested in playing video games:");
const interestedUsers = findUsersInterestedInVideoGames(users);
console.log(interestedUsers);
