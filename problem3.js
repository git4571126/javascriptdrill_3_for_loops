function findUsersWithMasters(users) {
    const usersWithMasters = [];

    try {
        const userNames = Object.keys(users);
        
        for (let i = 0; i < userNames.length; i++) {
            const userName = userNames[i];
            const user = users[userName];
            
            if (user.qualification === "Masters") {
                usersWithMasters.push(userName);
            }
        }
    } catch (error) {
        console.error("Error while finding users with Masters degree:", error);
    }
    
    return usersWithMasters;
}

module.exports = { findUsersWithMasters };
