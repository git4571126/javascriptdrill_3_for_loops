function findUsersInGermany(users) {
    const usersInGermany = [];

    try {
        const userNames = Object.keys(users);
        
        for (let i = 0; i < userNames.length; i++) {
            const userName = userNames[i];
            const user = users[userName];
            
            if (user.nationality === "Germany") {
                usersInGermany.push(userName);
            }
        }
    } catch (error) {
        console.error("Error while finding users in Germany:", error);
    }
    
    return usersInGermany;
}

module.exports = { findUsersInGermany };
